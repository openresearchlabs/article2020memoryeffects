# PanelA
  Dissimilarity to the initial stable state through time for all ten
  communities, for three different memory strengths.
  We simulated ten communities of 15 species, each withrandom  interaction  matrices.
  A similar level of commensurate memory is applied to all ten communities. 
  Every community is initially in a stable state of the system, and a perturbation 
  is imposed by multiplying the growth rates of half of the species (b1, ... , b7) by 3.

                   This code solves an N species microbial community model
                   described by fractional differential equations:
                   D^mu(Xi)=Xi(bi.Fi-ki.Xi)
                   where Fi=\prod[Kik^n/(Kik^n+Xk^n)], k=1,...,N and k~=i
                   D is the fractional Caputo derivative and mu is its order                          

### Inputs (Except T, T1, and T2, Don't change the input to keep the equilibrium points)

                    mu - Order of derivatives, e.g. mu=0.7*ones(1,N);  % 1-Memory
                    ------------------------------------------------------------------
                    n -  Hill coefficient, e.g. n=4;                              
                    ------------------------------------------------------------------
                    N -  Number of Species, N=15;
                    ------------------------------------------------------------------
                    Ki - Death rate, e.g. Ki=1*ones(N,1); 
                    ------------------------------------------------------------------
                    T - Final time, e.g. T=200;
                    ------------------------------------------------------------------
                    T1 - When the pulse perturbation starts 
                    T2 - When the pulse perturbation ends
-----------------------------------------------------------------------------------
### Outputs
        t - Simulated time interval
        D - Dissimilarity to the initial stable state through time  
--------------------------------------------------------------------------------

# PanelB
  Time series of the community 6.
  We simulated ten communities of 15 species, each withrandom  interaction  matrices.
  A similar level of commensurate memory is applied to all ten communities. 
  Every community is initially in a stable state of the system, and a perturbation 
  is imposed by multiplying the growth rates of half of the species (b1, ... , b7) by 3.

                   This code solves an N species microbial community model
                   described by fractional differential equations:
                   D^mu(Xi)=Xi(bi.Fi-ki.Xi)
                   where Fi=\prod[Kik^n/(Kik^n+Xk^n)], k=1,...,N and k~=i
                   D is the fractional Caputo derivative and mu is its order                          

### Inputs (Except iR, T, T1, and T2, Don't change the input to keep the equilibrium points)

                    mu - Order of derivatives, e.g. mu=0.7*ones(1,N);  % 1-Memory
                    ------------------------------------------------------------------
                    n -  Hill coefficient, e.g. n=4;                              
                    ------------------------------------------------------------------
                    N -  Number of Species, N=15;
                    ------------------------------------------------------------------
                    Ki - Death rate, e.g. Ki=1*ones(N,1); 
                    ------------------------------------------------------------------
                    T - Final time, e.g. T=200;
                    ------------------------------------------------------------------
                    T1 - When the pulse perturbation starts 
                    T2 - When the pulse perturbation ends
                    ------------------------------------------------------------------
                    iR - Number of the random community, e.g. iR=6; The 6th community is choosen out of generated random communities 
-----------------------------------------------------------------------------------
### Outputs
        t - Simulated time interval
        RelX - Time series of relative abundance 
--------------------------------------------------------------------------------

# PanelC
  Community dissimilarity (Bray-Curtis) between the start and the end of   
  the simulation for all ten communities and different memory levels.

                   This code solves an N species microbial community model
                   described by fractional differential equations:
                   D^mu(Xi)=Xi(bi.Fi-ki.Xi)
                   where Fi=\prod[Kik^n/(Kik^n+Xk^n)], k=1,...,N and k~=i
                   D is the fractional Caputo derivative and mu is its order                          

### Inputs (Except T, T1, and T2, Don't change the input to keep the equilibrium points)

                    mu - Order of derivatives, e.g. mu=0.7*ones(1,N);  % 1-Memory
                    ------------------------------------------------------------------
                    n -  Hill coefficient, e.g. n=4;                              
                    ------------------------------------------------------------------
                    N -  Number of Species, N=15;
                    ------------------------------------------------------------------
                    Ki - Death rate, e.g. Ki=1*ones(N,1); 
                    ------------------------------------------------------------------
                    T - Final time, e.g. T=200;
                    ------------------------------------------------------------------
                    T1 - When the pulse perturbation starts 
                    T2 - When the pulse perturbation ends
-----------------------------------------------------------------------------------
### Outputs
        t - Simulated time interval
        D - Bray-Curtis between the start and the end of the simulation    
--------------------------------------------------------------------------------

# PanelD
  Dissimilarity to the initial stable state through time for one randomly 
  chosen community (community~6) and different memory strengths.

                   This code solves an N species microbial community model
                   described by fractional differential equations:
                   D^mu(Xi)=Xi(bi.Fi-ki.Xi)
                   where Fi=\prod[Kik^n/(Kik^n+Xk^n)], k=1,...,N and k~=i
                   D is the fractional Caputo derivative and mu is its order                          

### Inputs (Except iR, T, T1, and T2, Don't change the input to keep the equilibrium points)

                    mu - Order of derivatives, e.g. mu=0.7*ones(1,N);  % 1-Memory
                    ------------------------------------------------------------------
                    n -  Hill coefficient, e.g. n=4;                              
                    ------------------------------------------------------------------
                    N -  Number of Species, N=15;
                    ------------------------------------------------------------------
                    Ki - Death rate, e.g. Ki=1*ones(N,1); 
                    ------------------------------------------------------------------
                    T - Final time, e.g. T=200;
                    ------------------------------------------------------------------
                    T1 - When the pulse perturbation starts 
                    T2 - When the pulse perturbation ends
                    ------------------------------------------------------------------
                    iR - Number of the random community, e.g. iR=6; The 6th community is choosen out of generated random communities 
-----------------------------------------------------------------------------------
### Outputs
        t - Simulated time interval
        D - Dissimilarity to the initial stable state through time   
--------------------------------------------------------------------------------
